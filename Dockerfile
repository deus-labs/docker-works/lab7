FROM adoptopenjdk/openjdk11:x86_64-alpine-jre-11.0.19_7
WORKDIR /lab7
COPY build/libs/*.jar lab7.jar
CMD [ "java", "-jar", "lab7.jar" ]